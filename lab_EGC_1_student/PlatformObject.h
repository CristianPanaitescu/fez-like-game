#pragma once
#include "PlayerObject.h"

#define BASE_LEVEL 0
#define	LEVEL_HEIGHT 30
#define LEVEL_OFFSET 4
#define BASE_WIDTH 400
#define BASE_HEIGHT 10

#define PLATFORM_MIN_X 20
#define PLATFORM_MAX_X 40
#define PLATFORM_MIN_Y 10
#define PLATFORM_MAX_Y 30
#define PLATFORM_MIN_Z 20
#define PLATFORM_MAX_Z 40

enum Type
{
	TypeNormal,
	TypeMovingX,
	TypeMovingY,
	TypeMovingZ,
	TypeCheckpoint,
	TypeGravity,
	TypeDouble
};

class PlatformObject :
	public SceneObject
{
public:
	PlatformObject();
	PlatformObject(lab::BlackBox* blackBox, int level);
	~PlatformObject();

	virtual void		Update() override;

	Type				GetType() { return m_type; };
	bool				IsPlayerOnPlatform() { return m_playerOnPlatform; }
	void				PutPlayerOnPlatform() { m_playerOnPlatform = true; }
	void				SetNoPlayerOnPlatform() { m_playerOnPlatform = false; }
	Vector3f			GetOffset();

private:

	void				GenerateBasePlatform();
	void				GenerateRandomPlatform();

	int					m_level;
	Type				m_type;
	float				m_speed;
	Vector3f			m_velocity;
	bool				m_movingDirection;
	bool				m_playerOnPlatform;

};

