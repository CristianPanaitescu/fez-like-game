
#include "lab_glut.hpp"
#include "lab_camera.hpp"
#include "PlayerObject.h"
#include "PlatformObject.h"


/*
TODO:
DONE 1. perspective pe o zona in dreapta + on/off option
2. background change color based on level u are
3. powerups: gravity and double jump
4. moving platforms (vertical or orizontal)
5. colored ball with rotation while moving
6. move left-right with acceleration
7. maybe: reset + dificulties
8. die mechanic
9. checkpointul
 
*/
//time
#include <ctime>

#define PERSPECTIVE_VIEWPORT_WIDTH	300
#define	LEVELS_NO					100
#define	PLATFORMS_PER_LEVEL			2
#define KEY_ESCAPE					27
#define PLAYERS						2

class Laborator5
	: public lab::glut::WindowListener
{
	private:
		// The BlackBox hides some functionality that we'll learn in the next course
		lab::BlackBox m_blackBox;

		bool keyState[256];
		bool specialKeyState[256];

		// Objects
		PlayerObject*					m_player1;
		PlayerObject*					m_player2;
		std::vector<PlatformObject*>	m_platforms;

		lab::Segments *obiect1;

		// helper variables for third person camera drawing
		bool thirdPersonCamera;
		bool alwaysDrawTarget;

		// Projection matrix
		bool isPerspectiveProjection;
		glm::mat4 projectionMatrix;
		glm::mat4 projectionMatrixPersp;
		float FoV;
		float zNear, zFar;
		float aspectRatio;
		float orthoLeft, orthoRight, orthoTop, orthoBottom;
		float orthoDistance;
		float orthoWidthWithPerspective;
		float orthoWidthWithoutPerspective;

		
		bool m_reset;
		Vector3f offset = Vector3f(0.f, 0.f, 0.f);

		bool m_mpl;
		// Camera
		lab::Camera m_cameraOrtho;
		lab::Camera m_cameraOrtho2;
		lab::Camera m_cameraPersp;

	public:
		Laborator5()
		{
			m_mpl = PLAYERS == 1 ? false : true;
			Init();

			//player perspective TODO
			//m_cameraPersp.set(glm::vec3(0, 0, 100), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
			// initializa all key states to not pressed
			memset(keyState, 0, 256);
			memset(specialKeyState, 0, 256);

			// helper variables for third person camera
			thirdPersonCamera = false;
			alwaysDrawTarget = false;

			// Initialize default projection values
			zNear = 0.1f;
			zFar = 1000.0f;
			FoV = 100.0f;
			aspectRatio = 0.4285f;

			orthoDistance = 5.f;
			orthoWidthWithPerspective = 38;
			orthoWidthWithoutPerspective = 24;
			orthoLeft = -orthoWidthWithPerspective * orthoDistance;
			orthoRight = orthoWidthWithPerspective * orthoDistance;
			orthoBottom = -25 * orthoDistance;
			orthoTop = 25 * orthoDistance;

			// value may be used for updating the projection when reshaping the window
			isPerspectiveProjection = true;

			projectionMatrix = glm::ortho(orthoLeft, orthoRight, orthoBottom, orthoTop, 1.f, zFar);
			projectionMatrixPersp = glm::perspective(FoV, aspectRatio, zNear, zFar);
		}

		void Init()
		{
			m_reset = false;
			m_player1 = new PlayerObject(&m_blackBox, &m_cameraOrtho);
			m_player2 = new PlayerObject(&m_blackBox, &m_cameraOrtho2);
			
			m_platforms.clear();
			m_platforms.push_back(new PlatformObject(&m_blackBox, BASE_LEVEL));

			for (int level = 0; level < LEVELS_NO; level++)
			{
				for (int index = 0; index < PLATFORMS_PER_LEVEL; index++)
				{
					m_platforms.push_back(new PlatformObject(&m_blackBox, level));
				}
			}

			// init camera
			m_cameraOrtho.set(glm::vec3(0, 0, 500), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
			m_cameraOrtho2.set(glm::vec3(0, 0, 500), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

			m_cameraOrtho.SetPositionState(CameraPositionStateFront);
			m_cameraOrtho2.SetPositionState(CameraPositionStateFront);

			//debug perspective
			m_cameraPersp.set(glm::vec3(250, 250, 250), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		}

		~Laborator5()
		{
		}

		void computePerspectiveProjection()
		{
			projectionMatrix = glm::perspective(FoV, aspectRatio, zNear, zFar);
		}

		void computeOrthograhicProjection()
		{
			projectionMatrix = glm::ortho(orthoLeft,orthoRight, orthoBottom, orthoTop , zNear , zFar);
		}

		void treatInput(PlayerObject* m_player, lab::Camera& m_cameraOrtho, char* keys)
		{
			float frameTime = m_blackBox.getFrameTimeSeconds();
			float moveSpeed = frameTime * 60;

			float rotateSpeedOX = frameTime / 2;
			float rotateSpeedOY = frameTime / 2;
			float rotateSpeedOZ = frameTime / 2;

			m_player->SetState(MovingStateWait);

			if (keyState[keys[0]])
			{
				m_player->SetState(MovingStateLeft);
				switch (m_cameraOrtho.GetPositionState())
				{
				case CameraPositionStateFront:
					offset = Vector3f(-11.f, 0.f, 0.f);
					break;
				case CameraPositionStateLeft:
					offset = Vector3f(0.f, 0.f, 11.f);	
					break;
				case CameraPositionStateBack:
					offset = Vector3f(11.f, 0.f, 0.f);
					break;
				case CameraPositionStateRight:
					offset = Vector3f(0.f, 0.f, -11.f);
					break;
				}
			}

			if (keyState[keys[1]])
			{
				m_player->SetState(MovingStateRight);
				switch (m_cameraOrtho.GetPositionState())
				{
				case CameraPositionStateFront:
					offset = Vector3f(11.f, 0.f, 0.f);
					break;
				case CameraPositionStateLeft:
					offset = Vector3f(0.f, 0.f, 11.f);
					break;
				case CameraPositionStateBack:
					offset = Vector3f(-11.f, 0.f, 0.f);
					break;
				case CameraPositionStateRight:
					offset = Vector3f(0.f, 0.f, -11.f);
					break;
				}
			}

			if (keyState['s'])
			{
				if (m_player->GetVerticalSpeed() == 0)
					m_player->SetPosition(m_player->GetPosition() - Vector3f(0.f, 2.f, 0.f));
			}

			if (!keyState[keys[4]])
			{
				m_player->m_released = true;
			}
			if (keyState[keys[4]])
			{
				if (m_player->GetVerticalSpeed() == 0)
				{
					m_player->SetVerticalSpeed(JUMP_SPEED);
					m_player->m_released = false;
				}
				else if (m_player->m_doubleLeft > 0 && !m_player->m_alreadyJumped2Times && m_player->m_released)
				{
					m_player->m_doubleLeft--;
					if (m_player->m_doubleLeft == 0)
						m_player->m_segm->setColor(0.4f, 0.f, 0.f);
					m_player->m_alreadyJumped2Times = true;
					m_player->SetVerticalSpeed(JUMP_SPEED);
				}
			}

			if (offset != Vector3f(0.f, 0.f, 0.f))
				m_cameraPersp.set(m_player->GetPosition() + offset, m_player->GetPosition() + offset + offset, glm::vec3(0, 1, 0));

			// Camera Translation
			if (keyState['i']) { 
				m_cameraPersp.translateForward(moveSpeed); }
			if (keyState['j']) { m_cameraPersp.translateRight(-moveSpeed); }
			if (keyState['k']) { m_cameraPersp.translateForward(-moveSpeed); }
			if (keyState['l']) { m_cameraPersp.translateRight(moveSpeed); }
			if (keyState['o']) { m_cameraPersp.translateUpword(moveSpeed); }
			if (keyState['u']) { m_cameraPersp.translateUpword(-moveSpeed); }

			// Camera Rotate FPS
			if (specialKeyState[GLUT_KEY_LEFT]) { m_cameraPersp.rotateFPS_OY(rotateSpeedOY); }
			if (specialKeyState[GLUT_KEY_RIGHT]) { m_cameraPersp.rotateFPS_OY(-rotateSpeedOY); }

			if (specialKeyState[GLUT_KEY_UP]) { m_cameraPersp.rotateFPS_OX(rotateSpeedOX); }
			if (specialKeyState[GLUT_KEY_DOWN]) { m_cameraPersp.rotateFPS_OX(-rotateSpeedOX); }

			if (keyState['-'])
			{
				orthoDistance *= 1.01f;
				
			}
			if (keyState['+'])
			{
				orthoDistance *= 0.99f;
			}

			if (keyState[keys[2]]) { m_cameraOrtho.setState(CameraStateRotateLeft); }
			if (keyState[keys[3]]) { m_cameraOrtho.setState(CameraStateRotateRight); }
			if (keyState['v']) 
			{ 
				isPerspectiveProjection = true;
				orthoLeft = -orthoWidthWithPerspective * orthoDistance;
				orthoRight = orthoWidthWithPerspective * orthoDistance;
				projectionMatrix = glm::ortho(orthoLeft, orthoRight, orthoBottom, orthoTop, -1000.f, zFar);
			}
			if (keyState['c'])
			{ 
				isPerspectiveProjection = false; 
				orthoLeft = -orthoWidthWithoutPerspective * orthoDistance;
				orthoRight = orthoWidthWithoutPerspective * orthoDistance;
				projectionMatrix = glm::ortho(orthoLeft, orthoRight, orthoBottom, orthoTop, -1000.f, zFar);
			}
		}

		void ComputeCameraBox()
		{
			if (isPerspectiveProjection)
			{
				orthoLeft = -orthoWidthWithPerspective * orthoDistance;
				orthoRight = orthoWidthWithPerspective * orthoDistance;
				orthoBottom = -25 * orthoDistance;
				orthoTop = 25 * orthoDistance;
			}
			else
			{
				orthoLeft = -orthoWidthWithoutPerspective * orthoDistance;
				orthoRight = orthoWidthWithoutPerspective * orthoDistance;
				orthoBottom = -25 * orthoDistance;
				orthoTop = 25 * orthoDistance;
			}
			projectionMatrix = glm::ortho(orthoLeft, orthoRight, orthoBottom, orthoTop, -1000.f, zFar);
			
		}

		void Draw()
		{
			m_player1->Draw();
			if (m_mpl)
				m_player2->Draw();

			for (unsigned int i = 0; i < m_platforms.size(); i++)
				m_platforms[i]->Draw();

			m_blackBox.setModelMatrix(glm::mat4(1));
			m_blackBox.drawAxisSystem(glm::vec3(0, 0, 0), glm::vec3(1, 0, 0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1), 30, 5);

		}
		void CheckGravity(PlayerObject* m_player)
		{
			if (m_player->m_gravityTimer > 0)
				m_player->m_gravityTimer -= m_blackBox.getFrameTimeSeconds();
			else if (m_player->m_gravityTimer < 0) {
				m_player->m_segm->setColor(0.4f, 0.f, 0.f);
				m_player->m_gravityTimer = 0;
				m_player->m_gravity = GRAVITY;
			}

			if (m_player->m_doubleLeft > 0 && m_player->m_gravityTimer > 0)
				m_player->m_segm->setColor(0.2f, 1.f, 0.8f);
			else if (m_player->m_doubleLeft > 0)
				m_player->m_segm->setColor(0.f, 0.6f, 0.6f);
			else if (m_player->m_gravityTimer > 0)
				m_player->m_segm->setColor(0.f, 0.6f, 0.f);
		}

		void notifyBeginFrame()
		{
			CheckGravity(m_player1);
			if (m_mpl)
				CheckGravity(m_player2);

			ComputeCameraBox();

			m_player1->m_velocity = Vector3f(0, 0, 0);
			m_player2->m_velocity = Vector3f(0, 0, 0);

			char keys1[5] = { 'a', 'd', '1', '3', 32};
			// Treat continuous input
			treatInput(m_player1, m_cameraOrtho, keys1);
			char keys2[5] = { 'j', 'l', 'u', 'o', 'i' };
			if (m_mpl)
				treatInput(m_player2, m_cameraOrtho2, keys2);
			
			Vector3f platformOffeset = Vector3f(0, 0, 0);

			for (unsigned int i = 1; i < m_platforms.size(); i++)
			{
				m_platforms[i]->Update();
				if (m_platforms[i]->IsPlayerOnPlatform() && !m_mpl)
					platformOffeset = m_platforms[i]->GetOffset();
			}
			if (!m_mpl)
			{
				m_player1->PlatformRequestMove(platformOffeset);
			}
			m_player1->Update();
			m_player2->Update();
		
			
			m_cameraOrtho.Update(m_player1->GetPosition(), m_blackBox.getFrameTimeSeconds());
			m_cameraOrtho2.Update(m_player2->GetPosition(), m_blackBox.getFrameTimeSeconds());

		};

		void CreateViewPortForOrtho()
		{
			unsigned int width = lab::glut::getInitialWindowInformation().width;
			unsigned int height = lab::glut::getInitialWindowInformation().height;

			if (!isPerspectiveProjection)
			{
				if (m_mpl)
				{
					orthoWidthWithoutPerspective = 24;
					glViewport(0, 0, width / 2, height);
					m_blackBox.setViewMatrixOrtho(m_cameraOrtho.getViewMatrix());
					m_blackBox.setProjectionMatrixOrtho(projectionMatrix);
					Draw();

					glViewport(width / 2, 0, width / 2, height);
					m_blackBox.setViewMatrixOrtho(m_cameraOrtho2.getViewMatrix());
					m_blackBox.setProjectionMatrixOrtho(projectionMatrix);
					Draw();
				}
				else
				{
					orthoWidthWithoutPerspective = 48;
					glViewport(0, 0, width, height);
					m_blackBox.setViewMatrixOrtho(m_cameraOrtho.getViewMatrix());
					m_blackBox.setProjectionMatrixOrtho(projectionMatrix);
					Draw();
				}
				
			}
			else
			{
				glViewport(0, 0, width - PERSPECTIVE_VIEWPORT_WIDTH, height);
				m_blackBox.setViewMatrixOrtho(m_cameraOrtho.getViewMatrix());
				m_blackBox.setProjectionMatrixOrtho(projectionMatrix);
				Draw();
			}

			
		}

		void CreateViewPortForPerspective()
		{
			unsigned int width = lab::glut::getInitialWindowInformation().width;
			unsigned int height = lab::glut::getInitialWindowInformation().height;

			glViewport(width - PERSPECTIVE_VIEWPORT_WIDTH, 0, PERSPECTIVE_VIEWPORT_WIDTH, height);

			m_blackBox.setViewMatrixPersp(m_cameraPersp.getViewMatrix());
			m_blackBox.setProjectionMatrixPersp(projectionMatrixPersp);
			Draw();
		}

		// Called every frame to draw
		void notifyDisplayFrame()
		{
			// Clear Color Buffer with the specified color
			glClearColor(1, 1, 1, 0);
			glClear(GL_COLOR_BUFFER_BIT);
			m_blackBox.notifyDisplay();

			CreateViewPortForOrtho();
			

			if (isPerspectiveProjection)
			{
				CreateViewPortForPerspective();
			}
		}

		void CheackForColisions(PlayerObject* m_player, lab::Camera m_cameraOrtho)
		{
			if (m_player->GetVerticalSpeed() > 0)
			{
				m_player->DecreaseVerticalSpeed();
				return;
			}

			//player stats
			Vector3f ppos = m_player->GetPosition();
			Vector3f pdim = m_player->GetDimension();

			std::vector<PlatformObject*> platformsToCheck;

			
			const bool frontOrBack = m_cameraOrtho.GetPositionState() == CameraPositionStateFront ||
									m_cameraOrtho.GetPositionState() == CameraPositionStateBack;
			const float epsilon = 1 * (fabs(m_player->GetVerticalSpeed()) + 1);
			for (unsigned int i = 0; i < m_platforms.size(); i++)
			{
				//platform stats
				Vector3f pos = m_platforms[i]->GetPosition();
				Vector3f dim = m_platforms[i]->GetDimension();
				m_platforms[i]->SetNoPlayerOnPlatform();
				if (frontOrBack)
				{
					if (ppos.x + pdim.x / 2 > pos.x - dim.x / 2 && ppos.x + pdim.x / 2 < pos.x + dim.x / 2 ||
						ppos.x - pdim.x / 2 > pos.x - dim.x / 2 && ppos.x - pdim.x / 2 < pos.x + dim.x / 2)
					{
						if (fabs((ppos.y - pdim.y / 2) - (pos.y + dim.y / 2)) <= epsilon)
						{
							platformsToCheck.push_back(m_platforms[i]);
						}
					}
				}
				else
				{
					if (ppos.z + pdim.z / 2 > pos.z - dim.z / 2 && ppos.z + pdim.z / 2 < pos.z + dim.z / 2 ||
						ppos.z - pdim.z / 2 > pos.z - dim.z / 2 && ppos.z - pdim.z / 2 < pos.z + dim.z / 2)
					{
						if (fabs((ppos.y - pdim.y / 2) - (pos.y + dim.y / 2)) <= epsilon)
						{
							platformsToCheck.push_back(m_platforms[i]);
						}
					}
				}
			}

			if (platformsToCheck.empty()) // no collision found
				m_player->DecreaseVerticalSpeed();
			else
			{
				Vector3f pos = platformsToCheck[0]->GetPosition();
				Vector3f dim = platformsToCheck[0]->GetDimension();
			
				if (m_player->GetVerticalSpeed() < -5)
					m_reset = true;
				m_player->SetVerticalSpeed(0);
				platformsToCheck[0]->PutPlayerOnPlatform();
				m_player->m_alreadyJumped2Times = false;
				NotifyPlayerSpecialPlatform(m_player, platformsToCheck[0]->GetType());
				if (frontOrBack)
				{
					if (m_platforms[0] != platformsToCheck[0])
						m_player->SetPosition(Vector3f(ppos.x, pos.y + dim.y / 2 + pdim.y / 2, pos.z));
				}
				else
				{
					if (m_platforms[0] != platformsToCheck[0])
						m_player->SetPosition(Vector3f(pos.x, pos.y + dim.y / 2 + pdim.y / 2, ppos.z));
				}
			}
			platformsToCheck.clear();
			
		}

		void NotifyPlayerSpecialPlatform(PlayerObject* m_player, Type platformType)
		{
			switch (platformType)
			{
			case TypeCheckpoint:
				m_player->m_checkpoint = m_player->GetPosition();
				break;
			case TypeDouble:
				m_player->m_doubleLeft = 5;
				break;
			case TypeGravity:
				m_player->m_gravityTimer = 10.f;
				m_player->m_gravity = 0.5 * GRAVITY;
				break;
			}
		}

		// Called when the frame ended
		void notifyEndFrame() {
			CheackForColisions(m_player1, m_cameraOrtho);
			if (m_mpl)
				CheackForColisions(m_player2, m_cameraOrtho2);
		}

		// A key was pressed
		void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y)
		{
			keyState[key_pressed] = 1;

			if (key_pressed == KEY_ESCAPE)
			{
				lab::glut::close();
			}

			if (key_pressed <= '9' && key_pressed >= '0')
				thirdPersonCamera = true;

			if (key_pressed == 't')
			{
				alwaysDrawTarget = !alwaysDrawTarget;
			}

			// TODO
			// Set the projection matrix to Orthographic
			if (key_pressed == 'o')
			{
				computeOrthograhicProjection();	//DONE 
			}

			// TODO
			// Set the projection matrix to Perspective 
			if (key_pressed == 'p')
			{
				computePerspectiveProjection(); //DONE 
			}

			if (key_pressed == 'b')
			{
				m_mpl = m_mpl ? false : true;
			}

			if (!m_mpl)
			{
				if (key_pressed == 'z' || (m_reset && m_player1->m_checkpoint != Vector3f(0, 0, 0)))
				{
					m_player1->SetPosition(m_player1->m_checkpoint);
					m_reset = false;
				}

				if (key_pressed == 'r' || m_reset)
				{
					Init();
					m_reset = false;
				}
			}
		}

		// When a key was released
		void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y)
		{
			keyState[key_released] = 0;

			// Disable drawing of camera target if no longer moving in ThirdPerson
			if (thirdPersonCamera)
			{
				char sum = 0;
				for (unsigned int i = 0; i <= 9; i++) {
					sum += keyState[i + '0'];
				}
				if (!sum) thirdPersonCamera = false;
			}

		}

		// Special key pressed like the navigation arrows or function keys F1, F2, ...
		void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y)
		{
			specialKeyState[key_pressed] = 1;

			switch (key_pressed)
			{
			case GLUT_KEY_F1: {
				lab::glut::enterFullscreen();
				break;
			}

			case GLUT_KEY_F2: {
				lab::glut::exitFullscreen();
				break;
			}

			case GLUT_KEY_F5: {
				m_blackBox.LoadShader();
				break;
			}

			default:
				break;
			}
		}

		// Called when a special key was released
		void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y)
		{
			specialKeyState[key_released] = 0;
		}

		//---------------------------------------------------------------------
		// Function called when the windows was resized
		void notifyReshape(int width, int height, int previos_width, int previous_height)
		{
			//m_blackBox needs to know
			m_blackBox.notifyReshape(width, height);
			aspectRatio = (float)width / height;

			// TODO - [OPTIONAL]
			// Recompute the orthographic or perspective matrices

		}

		//---------------------------------------------------------------------
		// Input function

		// Mouse drag, mouse button pressed 
		void notifyMouseDrag(int mouse_x, int mouse_y) { }

		// Mouse move without pressing any button
		void notifyMouseMove(int mouse_x, int mouse_y) { }

		// Mouse button click
		void notifyMouseClick(int button, int state, int mouse_x, int mouse_y) { }

		// Mouse scrolling
		void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y) { }
};

int main()
{
	// Initialize GLUT: window + input + OpenGL context
	lab::glut::WindowInfo window(std::string("Fez-like Game"), 1366, 700, 0, 0, true);
	lab::glut::ContextInfo context(3, 3, false);
	lab::glut::FramebufferInfo framebuffer(true, true, false, false);
	lab::glut::init(window, context, framebuffer);

	//generate new random seed
	srand((unsigned int)time(NULL));

	// Initialize GLEW + load OpenGL extensions 
	glewExperimental = true;
	glewInit();
	std::cout << "[GLEW] : initializare" << std::endl;

	// Create a new instance of Lab and listen for OpenGL callback
	// Must be created after GLEW because we need OpenGL extensions to be loaded

	Laborator5 *lab5 = new Laborator5();
	lab::glut::setListener(lab5);

	// Enter loop
	lab::glut::run();

	return 0;
}