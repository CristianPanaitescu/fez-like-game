#version 330

// Data from vertex shader
in vec3 color;
in float alpha;

// Output data from fragment shader
layout(location = 0) out vec4 out_color;

void main()
{
	out_color = vec4(color, 0);
}