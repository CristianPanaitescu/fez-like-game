#version 330

layout(location = 0) in vec3 in_position;		

uniform float in_alpha;
uniform vec3 in_color;	
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

out vec3 color;
out float alpha;

void main()
{
	color = in_color;
	alpha = in_alpha;
	gl_Position = Projection * View * Model * vec4(in_position, 1); 
}
