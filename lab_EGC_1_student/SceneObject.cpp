#include "SceneObject.h"


SceneObject::SceneObject()
{
}


SceneObject::~SceneObject()
{
}

void SceneObject::Update() {

}

void SceneObject::Draw() {

	m_transformation = glm::mat4(1);
	m_blackBox->setModelMatrix(MyTranslate(m_position) * MyIdentity());
	m_blackBox->drawSegments(m_segm); 
}