#pragma once
#include "SceneObject.h"
#include "lab_camera.hpp"

#define GRAVITY 5.f
#define JUMP_SPEED 2.f

enum MovingState
{
	MovingStateLeft,
	MovingStateRight,
	MovingStateWait,
};

class PlayerObject :
	public SceneObject
{
public:
	PlayerObject();
	PlayerObject(lab::BlackBox* blackBox, lab::Camera* camera);
	~PlayerObject();

	virtual void		Update();
	Vector3f			m_velocity;
	float				m_gravity;

	void				SetVerticalSpeed(float val) { m_verticalSpeed = val; }
	float				GetVerticalSpeed() { return m_verticalSpeed; }
	void				DecreaseVerticalSpeed();
	void				SetState(MovingState state);
	void				PlatformRequestMove(Vector3f offset);
	void				ResetPlatformMove() { m_platformOffset = Vector3f(0.f, 0.f, 0.f); }

	Vector3f			m_checkpoint;
	int					m_doubleLeft;
	bool				m_alreadyJumped2Times;
	bool				m_released;
	float				m_gravityTimer;

private:

	void				UpdateVelocity();
	void				Init();

	float				m_verticalSpeed;
	MovingState			m_movingState;
	lab::Camera*		m_camera;

	Vector3f			m_platformOffset;
};

