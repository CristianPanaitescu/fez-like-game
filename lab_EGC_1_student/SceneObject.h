#pragma once

#include "lab_blackbox.hpp"
#include "RNG.h"

#define PI 3.1415926435f

typedef glm::vec2 Vector2f;
typedef glm::vec3 Vector3f;

class SceneObject
{
public:
	SceneObject();
	~SceneObject();

	static glm::mat4 MyIdentity(){
		return glm::mat4(1);
	}
	static glm::mat4 MyTranslate(float tx, float ty, float tz){
		return glm::transpose(glm::mat4(
			1, 0, 0, tx,
			0, 1, 0, ty,
			0, 0, 1, tz,
			0, 0, 0, 1));
	}

	static glm::mat4 MyTranslate(Vector3f v){
		return glm::transpose(glm::mat4(
			1, 0, 0, v.x,
			0, 1, 0, v.y,
			0, 0, 1, v.z,
			0, 0, 0, 1));
	}

	virtual void			Update();
	virtual void			Draw();

	void					SetPosition(Vector3f position) { m_position = position; }
	Vector3f				GetPosition() { return m_position; }

	void					SetRotation(Vector3f rotation) { m_rotation = rotation; }
	Vector3f				GetRotation() { return m_rotation; }

	void					SetDimension(Vector3f dimension) { m_dimension = dimension; }
	Vector3f				GetDimension() { return m_dimension; }

	void					SetAlpha(float alpha) { m_alpha = alpha; }

	lab::Segments*			m_segm;
	glm::mat4				m_transformation;
	lab::BlackBox*			m_blackBox;

protected:

	Vector3f				m_position;
	Vector3f				m_rotation;
	Vector3f				m_dimension;
	float					m_alpha;
	
};

