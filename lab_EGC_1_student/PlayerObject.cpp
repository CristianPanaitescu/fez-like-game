#include "PlayerObject.h"


PlayerObject::PlayerObject() 
{
}

PlayerObject::PlayerObject(lab::BlackBox* blackBox, lab::Camera* camera)
{
	m_blackBox = blackBox;
	m_camera = camera;
	m_gravity = GRAVITY;
	m_alpha = 0.4f;

	m_doubleLeft = 0;
	m_gravityTimer = 0;
	m_alreadyJumped2Times = false;
	m_released = true;
	m_checkpoint = Vector3f(0, 0, 0);
	Init();
}

PlayerObject::~PlayerObject()
{
}

void PlayerObject::Update()
{
	if (m_camera->GetState() != CameraStateWait)
		return;

	switch (m_movingState)
	{
	case MovingStateLeft:
		m_velocity = Vector3f(-1.f, 0.f, 0.f);
		break;
	case MovingStateRight:
		m_velocity = Vector3f(1.f, 0.f, 0.f);
		break;
	}

	UpdateVelocity();

	float dtime = m_blackBox->getFrameTimeSeconds();
	m_position += m_platformOffset;
	m_position += m_velocity * dtime * 100.f;
	
}

void PlayerObject::SetState(MovingState state)
{
	if (m_movingState == MovingStateLeft && state == MovingStateRight ||
		m_movingState == MovingStateRight && state == MovingStateLeft)
		m_movingState = MovingStateWait;
	else
		m_movingState = state;
}

void PlayerObject::UpdateVelocity()
{
	switch (m_camera->GetPositionState())
	{
		case CameraPositionStateFront:
			break;
		case CameraPositionStateRight:
		{
			Vector3f newVelocity;
			newVelocity.x = -m_velocity.z;
			newVelocity.y = m_velocity.y;
			newVelocity.z = m_velocity.x;
			m_velocity = newVelocity;
			break;
		}
		case CameraPositionStateBack:
		{
			Vector3f newVelocity;
			newVelocity.x = - m_velocity.x;
			newVelocity.y = m_velocity.y;
			newVelocity.z = - m_velocity.y;
			m_velocity = newVelocity;
			break;
		}
		case CameraPositionStateLeft:
		{
			Vector3f newVelocity;
			newVelocity.x = m_velocity.z;
			newVelocity.y = m_velocity.y;
			newVelocity.z = - m_velocity.x;
			m_velocity = newVelocity;
			break;
		}
	}
	m_velocity.y = m_verticalSpeed;	
}

void PlayerObject::DecreaseVerticalSpeed()
{
	m_verticalSpeed -= m_blackBox->getFrameTimeSeconds() * m_gravity;
}

void PlayerObject::PlatformRequestMove(Vector3f offset)
{
	m_platformOffset = offset;
}

void PlayerObject::Init()
{
	m_segm = m_blackBox->myLoadCube_as_Segments(0, 0, 0, 20);
	m_position = Vector3f(0, 15, 0);
	m_dimension = Vector3f(20.f, 20.f, 20.f);
	m_segm->setColor(0.4f, 0.f, 0.f);
	m_verticalSpeed = 0;
}


