#include "PlatformObject.h"

PlatformObject::PlatformObject()
{
}

PlatformObject::PlatformObject(lab::BlackBox* blackBox, int level)
{
	m_blackBox = blackBox;
	m_level = level;
	m_alpha = 1.f;

	if (m_level == BASE_LEVEL)
		GenerateBasePlatform();
	else
		GenerateRandomPlatform();
	m_speed = 0;
	m_movingDirection = true;
	m_playerOnPlatform = false;
}

PlatformObject::~PlatformObject()
{
}

void PlatformObject::Update()
{
	if (m_blackBox->getFrameTimeSeconds() > 1)
		return;

	if (m_type == TypeMovingX || m_type == TypeMovingY || m_type == TypeMovingZ)
	{
		if (m_movingDirection == true)
		{
			m_speed += m_blackBox->getFrameTimeSeconds();
			if (m_speed > 1)
				m_movingDirection = false;
		}
		else
		{
			m_speed -= m_blackBox->getFrameTimeSeconds();
			if (m_speed < -1)
				m_movingDirection = true;
		}
		
		m_position += m_velocity * m_speed;
	}
	
}	

Vector3f PlatformObject::GetOffset()
{
	return m_velocity * m_speed;
}

void PlatformObject::GenerateBasePlatform()
{
	m_segm = m_blackBox->myLoadParal_as_Segments(0, 0, 0, BASE_WIDTH, BASE_HEIGHT, BASE_WIDTH);
	m_position = Vector3f(0.f, 0.f, 0.f);
	m_dimension = Vector3f(BASE_WIDTH, BASE_HEIGHT, BASE_WIDTH);
	m_segm->setColor(0.f, 0.f, 0.f);
}

void PlatformObject::GenerateRandomPlatform()
{
	m_position = Vector3f(
		float(RNG::Int(-BASE_WIDTH / 2, BASE_WIDTH / 2)),
		float(RNG::Int(-LEVEL_OFFSET / 4 + m_level * LEVEL_HEIGHT, LEVEL_OFFSET / 6 + m_level * LEVEL_HEIGHT)),
		float(RNG::Int(-BASE_WIDTH / 2, BASE_WIDTH / 2)));

	m_dimension = Vector3f(
		float(RNG::Int(PLATFORM_MIN_X, PLATFORM_MAX_X)),
		float(RNG::Int(PLATFORM_MIN_Y, PLATFORM_MAX_Y)),
		float(RNG::Int(PLATFORM_MIN_Z, PLATFORM_MAX_Z)));

	m_segm = m_blackBox->myLoadParal_as_Segments(0, 0, 0, m_dimension.x, m_dimension.y, m_dimension.z);

	m_velocity = Vector3f(0.f, 0.f, 0.f);

	int special = RNG::Int(0, 100);
	if (special < 70)
	{
		m_segm->setColor(0.f, 0.f, 0.f);
		m_type = TypeNormal;
	}
	else if (special >= 70 && special < 75)
	{
		m_segm->setColor(0.f, 0.f, 0.7f);
		m_type = TypeCheckpoint;
	}
	else if (special >= 75 && special < 80)
	{
		m_segm->setColor(0.7f, 0.f, 0.f);
		m_type = TypeMovingX;
		m_velocity.x = 1;
	}
	else if (special >= 80 && special < 85)
	{
		m_segm->setColor(0.7f, 0.f, 0.f);
		m_type = TypeMovingY;
		m_velocity.y = 1;
	}
	else if (special >= 85 && special < 90)
	{
		m_segm->setColor(0.7f, 0.f, 0.f);
		m_type = TypeMovingZ;
		m_velocity.z = 1;
	}
	else if (special >= 90 && special < 95)
	{
		m_segm->setColor(0.f, 0.7f, 0.f);
		m_type = TypeGravity;
	}
	else if (special >= 95 && special <= 100)
	{
		m_segm->setColor(0.f, 0.7f, 0.7f);
		m_type = TypeDouble;
	}	
}


