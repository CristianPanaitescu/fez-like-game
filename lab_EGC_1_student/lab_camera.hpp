//-------------------------------------------------------------------------------------------------
// Descriere: header camera
//
// Autor: Lucian Petrescu
// Data: 14 oct 2013
//-------------------------------------------------------------------------------------------------
#define PI 3.1415926435f
#define _USE_MATH_DEFINES
#include <math.h>

#pragma once
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"

enum CameraState
{
	CameraStateRotateLeft,
	CameraStateRotateRight,
	CameraStateWait
};

enum CameraPositionState
{
	CameraPositionStateFront,
	CameraPositionStateRight,
	CameraPositionStateBack,
	CameraPositionStateLeft
};

inline glm::vec3 RotateOY(const glm::vec3 Point, float radians)
{
	glm::vec3 R;
	R.x = Point.x * cos(radians) - Point.z * sin(radians);
	R.y = Point.y;
	R.z = Point.x * sin(radians) + Point.z * cos(radians);
	return R;
}

inline glm::vec3 RotateOX(const glm::vec3 Point, float radians)
{
	glm::vec3 R;
	R.y = Point.y * cos(radians) - Point.z * sin(radians);
	R.x = Point.x;
	R.z = Point.y * sin(radians) + Point.z * cos(radians);
	return R;
}

namespace lab
{
	class Camera
	{
	public:
		Camera()
		{
			position = glm::vec3(0, 0, 50);
			forward = glm::vec3(0, 0, -1);
			up		= glm::vec3(0, 1, 0);
			right	= glm::vec3(1, 0, 0);
			m_state = CameraStateWait;
			m_positionState = CameraPositionStateFront;
			m_angleToTravel = 0.f;
			m_speed = 3.f;
		}

		Camera(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up)
		{
			set(position, center,up);
		}

		~Camera()
		{ }

		// Update camera
		void set(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up)
		{
			this->position = position;
			forward = glm::normalize(center-position);
			right	= glm::cross(forward, up);
			this->up = glm::cross(right,forward);
		}

		void moveForwardKeepOY(float distance)
		{
			glm::vec3 dir = glm::normalize(glm::vec3(forward.x, 0, forward.z));
			// [OPTIONAL] - movement will keep the camera at the same height always
			// Example: If you rotate up/down your head and walk forward you will stil keep the same relative distance (height) to the ground!
			// Translate the camera using the DIR vector computed from forward
		}

		void translateForward(float distance)
		{
			// TODO
			// Translate the camera using the forward vector
			position = position + glm::normalize(forward) * distance;
		}

		void translateUpword(float distance)
		{
			// TODO
			// Translate the camera using the up vector
			position = position + glm::normalize(up) * distance;
		}

		void translateRight(float distance)
		{
			glm::vec3 dir = glm::normalize(glm::vec3(right.x, 0, right.z));
			position = position + glm::normalize(right) * distance;
			std::cout<<position.x<<endl;//<<forward<<up<<endl;
		}

		// Rotate the camera in FPS mode over the local OX axis
		void rotateFPS_OX(float angle)
		{
			forward = RotateOX(forward, angle);
			right = RotateOX(right, angle);
			up = glm::cross(right, forward);
		}

		void rotateFPS_OY(float angle)
		{
			forward = RotateOY(forward , angle);
			right = RotateOY(right , angle);
			up = glm::cross(right, forward);
		}

		void rotateFPS_OZ(float angle)
		{
		}

		void rotateTPS_OX(float angle)
		{
		}

		void rotateTPS_OY(float angle)
		{
			translateForward(position.z);
			rotateFPS_OY(angle);
			translateForward(position.z);
		}

		void rotateTPS_OZ(float angle)
		{
		}

		glm::mat4 getViewMatrix()
		{
			return glm::lookAt(position, position + forward, up);
		}

		glm::vec3 getPosition()
		{
			return position;
		}

		void	setPosition(glm::vec3 pos)
		{
			position = pos;
		}

		glm::vec3 getForward()
		{
			return forward;
		}

		CameraState getState()
		{
			return m_state;
		}

		void setState(CameraState state)
		{
			if (m_state != CameraStateWait)
				return;
			
			m_state = state;
			if (m_state == CameraStateRotateLeft)
				m_angleToTravel = -PI / 2;
			else
				m_angleToTravel = PI / 2;
		}

		CameraPositionState GetPositionState()
		{
			return m_positionState;
		}

		void SetPositionState(CameraPositionState pos)
		{
			m_positionState = pos;
		}

		CameraState GetState()
		{
			return m_state;
		}


		void ChangePositionStateLeft()
		{
			switch (m_positionState)
			{
			case CameraPositionStateFront:
				m_positionState = CameraPositionStateLeft;
				break;
			case CameraPositionStateLeft:
				m_positionState = CameraPositionStateBack;
				break;
			case CameraPositionStateBack:
				m_positionState = CameraPositionStateRight;
				break;
			case CameraPositionStateRight:
				m_positionState = CameraPositionStateFront;
				break;
			}
		}

		void ChangePositionStateRight()
		{
			switch (m_positionState)
			{
			case CameraPositionStateFront:
				m_positionState = CameraPositionStateRight;
				break;
			case CameraPositionStateLeft:
				m_positionState = CameraPositionStateFront;
				break;
			case CameraPositionStateBack:
				m_positionState = CameraPositionStateLeft;
				break;
			case CameraPositionStateRight:
				m_positionState = CameraPositionStateBack;
				break;
			}
		}

		void Update(glm::vec3 playerPosition, float dtime)
		{
			setPosition(playerPosition);

			if (m_state == CameraStateRotateLeft)
			{
				float angleToMove = dtime * m_speed;
				rotateTPS_OY(-angleToMove);
				m_angleToTravel += angleToMove;
				if (m_angleToTravel >= 0)
				{
					rotateTPS_OY(m_angleToTravel);
					m_angleToTravel = 0;
					m_state = CameraStateWait;
					ChangePositionStateLeft();
				}
			}
			else if (m_state == CameraStateRotateRight)
			{
				float angleToMove = dtime * m_speed;
				rotateTPS_OY(angleToMove);
				m_angleToTravel -= angleToMove;
				if (m_angleToTravel <= 0)
				{
					rotateTPS_OY(m_angleToTravel);
					m_angleToTravel = 0;
					m_state = CameraStateWait;
					ChangePositionStateRight();
				}
			}
		}

	private:
		glm::vec3 position;
		glm::vec3 forward;
		glm::vec3 right;
		glm::vec3 up;

		float m_speed;
		CameraState m_state;
		CameraPositionState m_positionState;
		float m_angleToTravel;
	};
}